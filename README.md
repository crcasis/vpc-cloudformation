# vpc-cloudformation

Creates an entire VPC from scratch for Lab or Permanent.

Create Details
Single VPC
3 Public Subnets
3 Private Subnets
Public Route Table
Private Route Table
Internet Gateway
Attached to the Public Route Table
Public Network ACL
Private Network ACL
